package demo.helloworld.controller;
import demo.helloworld.model.admin.Estate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class MainController {
//    @Autowired
//    private EstateServiceImpl estateService;
//
//    @Autowired
//    private ProjectServiceImpl projectService;
//
//    @Autowired
//    private NewsServiceImpl newsService;
//
//    @Autowired
//    private EmailService emailService;
//
//    @GetMapping("/")
//    public ModelAndView index() {
//        ModelAndView modelAndView = new ModelAndView ("user/index");
//        modelAndView.addObject ("estate",new Estate ());
//        return modelAndView;
//    }
//
//    @GetMapping("/search")
//    public ModelAndView search(@ModelAttribute("estate") Estate estate) {
//        ModelAndView modelAndView = new ModelAndView ("user/travel_destination");
//        modelAndView.addObject ("estate",estate);
//        return modelAndView;
//    }
//
    @GetMapping("/login")
    public String getLogin() {
    return "admin/login";
}

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }
//    @GetMapping("/403")
//    public String accessDenied() {
//        return "user/403";
//    }
//
//    @GetMapping("/about")
//    public String about() { return  "user/about"; }
//
//    @GetMapping("/estateNews")
//    public String estateNews() { return "user/estatenews"; }
//
//    @GetMapping("/projectNews")
//    public String projectNews() { return "user/projectnews"; }
//
//    @GetMapping("/contact")
//    public ModelAndView contact() {
//        ModelAndView modelAndView = new ModelAndView ("user/contact");
//        modelAndView.addObject ("contact",new Contact ());
//        return modelAndView;
//    }
//    @GetMapping("/estate")
//    public String estateList() {
//        return "user/travel_destination";
//    }
//
//    @PostMapping(value = "/sendmail")
//    public ModelAndView sendmail(@ModelAttribute("contact") Contact contact) {
//
//        ModelAndView modelAndView = new ModelAndView ("user/contact");
//        emailService.sendMail(contact.getEmail (), contact.getName (), contact.getAddress ());
//        modelAndView.addObject ("message","Thank you for contact us!!!");
//        return modelAndView;
//    }
//
//    @GetMapping("/elements")
//    public String elements() { return  "user/elements"; }
//
//
//    @GetMapping("/destination_details")
//    public ModelAndView destinationDetails(@RequestParam Long id) {
//        Estate estate = estateService.findById(id);
//        if (estate != null) {
//            ModelAndView modelAndView = new ModelAndView ("user/destination_details");
//            modelAndView.addObject ("estate",estate);
//            return modelAndView;
//        }
//        ModelAndView modelAndView = new ModelAndView("/error/404");
//        modelAndView.addObject("message","Data not found !");
//        return modelAndView;
//    }
//
//    @GetMapping("/newsDetail")
//    public ModelAndView showNewsDetail (@RequestParam Long id) {
//        News news = newsService.findById(id);
//        if (news != null) {
//            ModelAndView modelAndView = new ModelAndView ("user/newsDetail");
//            modelAndView.addObject ("news",news);
//            return modelAndView;
//        }
//        ModelAndView modelAndView = new ModelAndView("/error/404");
//        modelAndView.addObject("message","Data not found !");
//        return modelAndView;
//    }
}
